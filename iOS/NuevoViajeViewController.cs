using Foundation;
using System;
using UIKit;
using System.IO;
using Trips.Clases;

namespace Trips.iOS
{
    public partial class NuevoViajeViewController : UIViewController
    {
        public NuevoViajeViewController (IntPtr handle) : base (handle)
        {
            
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            guardarBarButtonItem.Clicked += GuardarBarButtonItem_Clicked;
        }

        void GuardarBarButtonItem_Clicked(object sender, EventArgs e)
        {
            string nombreArchivo = "viajes_db.sqlite";
            string rutaCarpeta = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "..", "Library");
            string rutaCompleta = Path.Combine(rutaCarpeta, nombreArchivo);

            var nuevoViaje = new Viaje
            {
                Nombre = ciudadTextField.Text,
                FechaInicio = (DateTime)fechaInicioDatePicker.Date,
                FechaRegreso = (DateTime)fechaFinDatePicker.Date
            };

            if (DatabaseHelper.Insertar(ref nuevoViaje, rutaCompleta))
                Console.WriteLine("Insert exitoso");
            else
                Console.WriteLine("Hubo un error");

        }
    }
}