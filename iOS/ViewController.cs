﻿using System;

using UIKit;

namespace Trips.iOS
{
    public partial class ViewController : UIViewController
    {

        public ViewController(IntPtr handle) : base(handle)
        {
            
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Perform any additional setup after loading the view, typically from a nib.
            inicioButton.TouchUpInside += InicioButton_TouchUpInside;
        }

        void InicioButton_TouchUpInside(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(usuarioEditText.Text) || string.IsNullOrEmpty(passwordEditText.Text))
            {
                
            }
            else{
                //TODO: agregar navegación
            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.		
        }
    }
}
