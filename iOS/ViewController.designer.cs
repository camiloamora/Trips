// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Trips.iOS
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        UIKit.UIButton Button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton inicioButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField passwordEditText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField usuarioEditText { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (inicioButton != null) {
                inicioButton.Dispose ();
                inicioButton = null;
            }

            if (passwordEditText != null) {
                passwordEditText.Dispose ();
                passwordEditText = null;
            }

            if (usuarioEditText != null) {
                usuarioEditText.Dispose ();
                usuarioEditText = null;
            }
        }
    }
}