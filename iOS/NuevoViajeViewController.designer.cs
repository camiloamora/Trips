// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Trips.iOS
{
    [Register ("NuevoViajeViewController")]
    partial class NuevoViajeViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField ciudadTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIDatePicker fechaFinDatePicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIDatePicker fechaInicioDatePicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem guardarBarButtonItem { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ciudadTextField != null) {
                ciudadTextField.Dispose ();
                ciudadTextField = null;
            }

            if (fechaFinDatePicker != null) {
                fechaFinDatePicker.Dispose ();
                fechaFinDatePicker = null;
            }

            if (fechaInicioDatePicker != null) {
                fechaInicioDatePicker.Dispose ();
                fechaInicioDatePicker = null;
            }

            if (guardarBarButtonItem != null) {
                guardarBarButtonItem.Dispose ();
                guardarBarButtonItem = null;
            }
        }
    }
}