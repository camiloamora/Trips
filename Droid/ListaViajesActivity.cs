﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Trips.Clases;

namespace Trips.Droid
{
    [Activity(Label = "ListaViajesActivity")]
    public class ListaViajesActivity : ListActivity
    {

        #region Propiedades

        static string nombreArchivo = "viajes_db.sqlite";
        static string rutaCarpeta = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        static string ruta = Path.Combine(rutaCarpeta, nombreArchivo);
        List<Viaje> viajes;

        #endregion


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            viajes = new List<Viaje>();
            viajes = DatabaseHelper.ListaViajes(ruta);

            var arrayAdapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItem1, viajes);
            ListAdapter = arrayAdapter;

        }

        protected override void OnRestart()
        {
            base.OnRestart();

            viajes = new List<Viaje>();
            viajes = DatabaseHelper.ListaViajes(ruta);

            var arrayAdapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItem1, viajes);
            ListAdapter = arrayAdapter;
        }
    }
}
