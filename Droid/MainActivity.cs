﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content;

namespace Trips.Droid
{
    [Activity(Label = "Trips", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        EditText nombreUsuarioEditText, passwordEditText;
        Button inicioSesionButton;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            nombreUsuarioEditText = FindViewById<EditText>(Resource.Id.usuarioEditText);
            passwordEditText = FindViewById<EditText>(Resource.Id.passwordEditText);
            inicioSesionButton = FindViewById<Button>(Resource.Id.inicioButton);

            inicioSesionButton.Click += InicioSesionButton_Clicked;
        }

        private void InicioSesionButton_Clicked(object sender, EventArgs argumentos)
        {
            if(string.IsNullOrEmpty(nombreUsuarioEditText.Text) || string.IsNullOrEmpty(passwordEditText.Text))
            {
                
            }
            else
            {
                Intent intent = new Intent(this, typeof(ListaViajesActivity));
                StartActivity(intent);
            }
        }
    }
}

