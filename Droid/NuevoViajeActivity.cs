﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Trips.Clases;

namespace Trips.Droid
{
    [Activity(Label = "NuevoViajeActivity")]
    public class NuevoViajeActivity : Activity
    {
        EditText lugarEditText;
        DatePicker fechaInicioPicker, fechaFinPicker;
        Button guardarButton;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.NuevoViaje);

            lugarEditText = FindViewById<EditText>(Resource.Id.lugarEditText);
            fechaInicioPicker = FindViewById<DatePicker>(Resource.Id.fachaInicioPicker);
            fechaFinPicker = FindViewById<DatePicker>(Resource.Id.fechaFinPicker);
            guardarButton = FindViewById<Button>(Resource.Id.Guardarbutton);

            guardarButton.Click += GuardarButton_Click;
        }

        void GuardarButton_Click(object sender, EventArgs e)
        {
            string nombreArchivo = "viajes_db.sqlite";
            string rutaCarpeta = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string ruta = Path.Combine(rutaCarpeta, nombreArchivo);

            var nuevoViaje = new Viaje()
            {
                Nombre = lugarEditText.Text,
                FechaInicio = fechaInicioPicker.DateTime,
                FechaRegreso = fechaFinPicker.DateTime
            };

            if(DatabaseHelper.Insertar(ref nuevoViaje, ruta))
                Toast.MakeText(this, "Registro insertado correctamente", ToastLength.Short).Show();
            else
                Toast.MakeText(this, "Hubo un error", ToastLength.Short).Show();
        }
    }
}
